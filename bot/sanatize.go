package bot

import "net/url"

var globallyBannedQueryParams = []string{
	"fbclid",
	"utm_source",
	"utm_medium",
	"utm_campaign",
	"utm_id",
}

func SanatizeURL(dirty string) string {
	u, err := url.Parse(dirty)
	if err != nil {
		return dirty
	}

	for _, param := range globallyBannedQueryParams {
		sanatizeParam(u, param)
	}

	switch u.Host {
	case "youtu.be", "youtube.com":
		sanatizeParam(u, "si")
	}

	return u.String()
}

func sanatizeParam(u *url.URL, param string) {
	q := u.Query()
	q.Del(param)
	u.RawQuery = q.Encode()
}
