# Social Decrapifier Signal Bot
*a signal bot for retreiving short videos from social media*

This is a simple bot to retreive social media videos. Send it a message with a link to a video on any supported social media site, and it will reply
with the video embedded, in an easily forwardable form. Great for decrapifying social media links before sending them to friends.

## usage
Build like a normal go program:

```
go build ./cmd/social-decrapifier-bot
```

configure with `social-decrapifier-bot.json` in the directory the program is started in:
```json
{
    // signal_account is the UUID or phone number for the signal account to use. must be registered on signald
    "signal_account": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",

    // s3 (optional) - if set, configures the s3 service to cache files with
    "s3": {
        "access_key_id": "xxxxxxxxxxxxxxxx",
        "secret_access_key": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "endpoint": "my.favorite.s3.provider",
        "bucket": "xxxxx",
        "public_url_base": "https://social-decrapifier.bot" // optional, if unset pre-signed download URLs will be sent. pre-signed download URLs are long and ugly, and expire in a week
    },

    // max_attachment_size_mb (optional, defaults to 100) - downloads exceeding this size will not be sent via signal. for direct message responses, a thumbnail will be sent if available, along with the s3 download link
    "max_attachment_size_mb": 100,

    // skip_cleanup (optional, default false) is useful for debugging yt-dlp issues. When enabled, temporary files will not be deleted after download attempts
    "skip_cleanup": false,

    // extra_ytdlp_options (optional, default []) - a list of yt-dlp options to pass to each invocation of yt-dlp. These will be added after the hard-coded options, which are:
    // "--no-playlist", "--match-filter", "!is_live", "-o", "%(id)s.%(ext)s"
    // as well as some per-domain flavor, see bot/yt-dlp.go for details
    "extra_ytdlp_options": ["--merge-output-format", "mp4/mkv", "--netrc"],

    // http_bind enables the REST server and specifies the address:port to bind to (golang bind formatting, leave address blank to bind to all)
    // expects form fields url and either notify_user or notify_group
    // example: curl -d "url=https://www.youtube.com/watch?v=xxxx" -d "notify_user=ae91e3d9-9d05-48ba-a68c-6a8783b10222" http://localhost:8080/api/job
    // remember to URL-encode the values
    "http_bind": ":8080",

    // debug (optional, default false) - enable debug logging
    "debug": false

}
```

expects to be run within a pipenv. A systemd service file might look like this:

```
[Unit]
Description=Social Decrapifier Bot
Wants=network.target
After=signald.service

[Service]
User=social-decrapifier-bot
Group=signald
ExecStart=/usr/bin/pipenv run /usr/local/bin/social-decrapifier-bot
Restart=always
RestartSec=30
WorkingDirectory=/home/social-decrapifier-bot/env

[Install]
WantedBy=multi-user.target
```

the program will install or update yt-dlp immediately on startup and every 3 days thereafter

**requires [signald](https://signald.org)**