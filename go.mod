module gitlab.com/thefinn93/social-decrapifier-signal-bot

go 1.14

require (
	github.com/labstack/echo/v4 v4.10.0
	github.com/minio/minio-go/v7 v7.0.39
	github.com/sirupsen/logrus v1.9.0
	github.com/stretchr/testify v1.8.1
	gitlab.com/signald/signald-go v0.0.0-20220316021442-3ccb424f5306
	mvdan.cc/xurls/v2 v2.4.0
)
