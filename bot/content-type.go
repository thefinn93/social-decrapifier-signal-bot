package bot

import (
	"path/filepath"

	log "github.com/sirupsen/logrus"
)

func getContentType(filename string) string {
	switch filepath.Ext(filename) {
	case ".webm":
		return "video/webm"
	case ".mp4":
		return "video/mp4"
	default:
		log.Warn("no known content type for ", filepath.Ext(filename))
		return ""
	}
}
