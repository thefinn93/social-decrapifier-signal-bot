package bot

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

// stringyInt is an int that can also be parsed from a JSON string via strconv.Parseint
type stringyInt int

type ydlInfoJSON struct {
	Extractor      string      `json:"extractor"`
	UploaderID     string      `json:"uploader_id"`
	Uploader       string      `json:"uploader"`
	Title          string      `json:"title"`
	DurationString string      `json:"duration_string"`
	Width          float64     `json:"width"`
	Height         float64     `json:"height"`
	ACodec         string      `json:"acodec"`
	VCodec         string      `json:"vcodec"`
	Channel        string      `json:"channel"`
	FPS            float64     `json:"fps"`
	Ext            string      `json:"ext"`
	ID             string      `json:"id"`
	Thumbnails     []thumbnail `json:"thumbnails"`
}

type thumbnail struct {
	URL        string     `json:"url"`
	Preference int        `json:"preference"`
	ID         string     `json:"id"`
	Height     stringyInt `json:"height"`
	Width      int        `json:"width"`
	Resolution string     `json:"resolution"`
}

const infoJSONFile = "media.info.json"

func getInfo(u string, dir string) (ydlInfoJSON, error) {
	if err := ytdlp(u, dir, "--write-info-json", "--skip-download", "-o", "media.%(ext)s"); err != nil {
		return ydlInfoJSON{}, err
	}

	f, err := os.Open(filepath.Join(dir, infoJSONFile))
	if err != nil {
		return ydlInfoJSON{}, err
	}
	defer f.Close()

	var info ydlInfoJSON
	if err = json.NewDecoder(f).Decode(&info); err != nil {
		return ydlInfoJSON{}, err
	}

	return info, nil
}

func ytdlp(u string, dir string, extraOpts ...string) error {
	args, err := ytdlpOptions(u, extraOpts...)
	if err != nil {
		return err
	}
	youtubedl := exec.Command("yt-dlp", args...)
	youtubedl.Dir = dir
	log.Debug("running command: ", youtubedl.Args)
	start := time.Now()
	out, err := youtubedl.Output()
	log.Debug("yt-dlp time ", time.Since(start))
	if err != nil {
		msg := string(out)
		if exitError, ok := err.(*exec.ExitError); ok {
			log.Warn("yt-dlp stdout:", string(out))
			log.Warn("yt-dlp stderr:", string(exitError.Stderr))
			version := ytdlpVersion()
			log.Warn("yt-dlp version:", version)
			msg = string(exitError.Stderr) + "\n\nyt-dlp version: " + version
		}
		return fmt.Errorf("error running yt-dlp:\n%s\n\n%v", msg, err)
	}

	return nil
}

func ytdlpOptions(u string, extra ...string) (args []string, err error) {
	parsedURL, err := url.Parse(u)
	if err != nil {
		return []string{}, fmt.Errorf("error parsing url %s: %v", u, err)
	}

	switch parsedURL.Host {
	case "www.tiktok.com", "tiktok.com":
		args = append(args, "--add-headers", "User-Agent:Mozilla/5.0")
	}

	args = append(args, "--no-playlist", "--match-filter", "!is_live", "-o", "%(id)s.%(ext)s")
	args = append(args, extra...)
	args = append(args, config.ExtraYtdlpOptions...)
	args = append(args, u)

	return args, nil
}

func (y ydlInfoJSON) getThumbnailURL() string {
	if len(y.Thumbnails) == 0 {
		return ""
	}

	u := ""
	var best int
	for _, thumbnail := range y.Thumbnails {
		if thumbnail.Preference <= best && u != "" {
			continue
		}

		// todo: should this be limited to YouTube?
		if thumbnail.Resolution == "" {
			continue
		}

		best = thumbnail.Preference
		u = thumbnail.URL
	}

	return u
}

func (s *stringyInt) UnmarshalJSON(data []byte) error {
	var i int
	if err := json.Unmarshal(data, &i); err != nil {
		if !bytes.HasPrefix(data, []byte("\"")) {
			return err
		}
	} else {
		stringy := stringyInt(i)
		s = &stringy
		return nil
	}

	var strval string
	if err := json.Unmarshal(data, &strval); err != nil {
		return err
	}

	parsed, err := strconv.ParseInt(strval, 10, 64)
	if err != nil {
		return err
	}

	stringy := stringyInt(parsed)
	s = &stringy

	return nil
}

func ytdlpVersion() string {
	version, err := exec.Command("yt-dlp", "--version").Output()
	if err != nil {
		return fmt.Sprintf("error checking yt-dlp version: %v", err)
	}
	return strings.TrimSpace(string(version))
}
