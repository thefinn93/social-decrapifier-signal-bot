package bot

import (
	"encoding/json"

	log "github.com/sirupsen/logrus"
	"gitlab.com/signald/signald-go/signald"
	client_protocol "gitlab.com/signald/signald-go/signald/client-protocol"
	v1 "gitlab.com/signald/signald-go/signald/client-protocol/v1"
	xurls "mvdan.cc/xurls/v2"
)

var (
	urlextractor = xurls.Strict()
	signaldConn  = &signald.Signald{}
)

// SignalListener listens for signald messages
func SignalListener() {
	if err := signaldConn.Connect(); err != nil {
		log.Fatal("failed to connect to signald: ", err)
	}

	incoming := make(chan client_protocol.BasicResponse)
	go signaldConn.Listen(incoming)

	subscribeReq := v1.SubscribeRequest{Account: config.SignalAccount}
	if err := subscribeReq.Submit(signaldConn); err != nil {
		log.Fatal("error connecting to signal account: ", err)
	}

	for msg := range incoming {
		if msg.Type != "IncomingMessage" {
			continue
		}

		var data v1.IncomingMessage
		if err := json.Unmarshal(msg.Data, &data); err != nil {
			log.Error("error parsing message from signald: ", err)
			continue
		}

		if data.DataMessage == nil {
			continue
		}

		urls := urlextractor.FindAllString(data.DataMessage.Body, -1)
		group := ""
		if data.DataMessage.GroupV2 != nil {
			group = data.DataMessage.GroupV2.ID
		}

		for _, u := range urls {
			log.Debug("queuing download for ", u)
			job := downloadJob{
				URL: SanatizeURL(u),
				Quote: v1.JsonQuote{
					Author: &v1.JsonAddress{
						UUID:   data.Source.UUID,
						Number: data.Source.Number,
					},
					ID:   data.Timestamp,
					Text: data.DataMessage.Body,
				},
				Group: group,
			}

			if len(downloadQueue) > 0 || inprogress {
				go job.react("⌛")
			}

			downloadQueue <- job
		}

		go func() {
			readReq := v1.MarkReadRequest{
				Account:    config.SignalAccount,
				To:         data.Source,
				Timestamps: []int64{data.Timestamp},
			}
			if err := readReq.Submit(signaldConn); err != nil {
				log.Error("error marking message read: ", err)
			}
		}()
	}
}
