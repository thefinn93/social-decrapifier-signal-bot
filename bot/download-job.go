package bot

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"

	minio "github.com/minio/minio-go/v7"
	"github.com/sirupsen/logrus"
	v1 "gitlab.com/signald/signald-go/signald/client-protocol/v1"
)

const (
	mb = 1024 * 1024
)

var footer = buildFooter()

type downloadJob struct {
	dir       string
	URL       string
	Quote     v1.JsonQuote
	lastReact string
	Group     string
}

func (d *downloadJob) Download() error {
	if err := d.react("👀"); err != nil {
		return fmt.Errorf("error reacting: %v", err)
	}

	parsedURL, _ := url.Parse(d.URL)
	for _, domain := range config.SkipDomains {
		if parsedURL.Host == domain {
			if err := d.react(""); err != nil {
				return err
			}
			return nil
		}
	}

	err := d.mkdir()
	if err != nil {
		return fmt.Errorf("error creating temporary download directory: %v", err)
	}
	if !config.SkipCleanup {
		defer d.cleanup()
	}

	infoJSON, err := getInfo(d.URL, d.dir)
	if err != nil {
		return fmt.Errorf("error scraping URL: %v", err)
	}

	var objectName string
	var size int64
	download := true
	filename := filepath.Join(d.dir, infoJSON.ID+"."+infoJSON.Ext)

	if minioClient != nil {
		objectNameParts := []string{}
		for _, part := range []string{infoJSON.Extractor, infoJSON.UploaderID, infoJSON.ID + "." + infoJSON.Ext} {
			if part == "" {
				continue
			}

			part = strings.ToLower(part)

			// clean any invalid characters from the filename part
			for _, c := range []string{"^", "*", "|", "\\", "/", "&", "\"", ";"} {
				part = strings.ReplaceAll(part, c, "-")
			}

			objectNameParts = append(objectNameParts, part)
		}

		logrus.Debug("object name: ", objectNameParts)

		objectName = strings.Join(objectNameParts, "/")

		info, err := minioClient.StatObject(context.Background(), config.S3.Bucket, objectName, minio.StatObjectOptions{})
		if err == nil {
			size = info.Size
			download = false
		}
	}

	if download {
		if err := d.react("⬇️"); err != nil {
			return fmt.Errorf("error reacting: %v", err)
		}

		if err := ytdlp(d.URL, d.dir, "--load-info-json", filepath.Join(d.dir, infoJSONFile)); err != nil {
			return err
		}

		if err := d.react("⬆️"); err != nil {
			logrus.Warnf("error reacting: %v", err)
		}

		if minioClient != nil {
			logrus.Debugf("uploading %s -> %s:%s", filename, config.S3.Bucket, objectName)
			_, err := minioClient.FPutObject(context.Background(), config.S3.Bucket, objectName, filename, minio.PutObjectOptions{
				ContentType: getContentType(filename),
			})
			if err != nil {
				return fmt.Errorf("error uploading file: %v", err)
			}
		}

		stat, err := os.Stat(filename)
		if err != nil {
			return fmt.Errorf("error stating file: %v", err)
		}

		size = stat.Size()
	}

	attachments := []*v1.JsonAttachment{}

	if size < mb*config.MaxAttachmentSizeMB {
		if !download {
			// file is small enough to send to Signal, and has been downloaded already. Retrieve it from S3 so it can be uploaded to Signal
			if err := minioClient.FGetObject(context.Background(), config.S3.Bucket, objectName, filename, minio.GetObjectOptions{}); err != nil {
				return fmt.Errorf("error downloading file: %v", err)
			}

			if err := os.Chmod(filename, 0644); err != nil {
				return fmt.Errorf("error setting mode on downloaded file: %v", err)
			}

			if err := d.react("⬆️"); err != nil {
				logrus.Warnf("error reacting: %v", err)
			}
		}

		attachments = append(attachments, &v1.JsonAttachment{
			Filename: filepath.Join(d.dir, infoJSON.ID+"."+infoJSON.Ext),
		})
	} else if d.Group != "" && d.Quote.ID > 0 {
		d.react("")
		return nil
	} else {
		thumbnailURL := infoJSON.getThumbnailURL()
		if thumbnailURL != "" {
			localThumbnail := filepath.Join(d.dir, "thumbnail.jpg")
			if err := downloadThumbnailURL(thumbnailURL, localThumbnail); err != nil {
				return fmt.Errorf("error creating thumbnail image: %v", err)
			}

			attachments = append(attachments, &v1.JsonAttachment{
				Filename: localThumbnail,
			})
		}
	}

	body := ""
	if d.Group == "" || d.Quote.ID == 0 {
		bodyparts := map[string]string{
			"original": d.URL,
			"title":    infoJSON.Title,
			"runtime":  infoJSON.DurationString,
			"channel":  infoJSON.Channel,
		}

		if infoJSON.Width > 0 || infoJSON.Height > 0 {
			bodyparts["resolution"] = fmt.Sprintf("%dx%d", int(infoJSON.Width), int(infoJSON.Height))
		}

		if infoJSON.ACodec != "" || infoJSON.VCodec != "" {
			bodyparts["codec"] = fmt.Sprintf("%s/%s", infoJSON.ACodec, infoJSON.VCodec)
		}

		if infoJSON.FPS > 0 {
			bodyparts["fps"] = fmt.Sprintf("%v", infoJSON.FPS)
		}

		var bodyBuilder strings.Builder

		if minioClient != nil {
			var link string
			if config.S3.PublicURLBase != "" {
				link = fmt.Sprintf("%s/%s", config.S3.PublicURLBase, objectName)
			} else {
				u, err := minioClient.PresignedGetObject(context.Background(), config.S3.Bucket, objectName, config.S3.PresignedLinkTTL.Duration, url.Values{})
				if err != nil {
					return err
				}
				link = u.String()
			}

			shortenedLink, err := shortenURL(link)
			if err != nil {
				logrus.WithError(err).Warn("unexpected error trying to shorten link. Using unshortened link")
				shortenedLink = link
			}

			bodyBuilder.WriteString(shortenedLink)
			bodyBuilder.WriteRune('\n')
			bodyBuilder.WriteRune('\n')
		}

		for key, value := range bodyparts {
			value = strings.TrimSpace(value)

			if value == "" {
				continue
			}

			if key != "" {
				bodyBuilder.WriteString(key)
				bodyBuilder.WriteString(": ")
			}

			bodyBuilder.WriteString(value)
			bodyBuilder.WriteRune('\n')
		}

		// bodyBuilder.WriteString("\n\n")
		// bodyBuilder.WriteString(footer)

		body = bodyBuilder.String()
	}

	err = d.reply(v1.SendRequest{
		Attachments: attachments,
		MessageBody: body,
	})
	if err != nil {
		return fmt.Errorf("error sending response to signal: %v", err)
	}

	if err := d.react("✅"); err != nil {
		return fmt.Errorf("error reacting: %v", err)
	}

	logrus.Debug("Successfully got ", d.URL)

	return nil
}

func (d *downloadJob) react(emoji string) error {
	logrus.Debug("reacting ", emoji, " for job ", d)

	if d.Quote.ID == 0 {
		return nil
	}

	remove := emoji == ""
	if remove {
		emoji = d.lastReact
		d.lastReact = ""
	} else {
		d.lastReact = emoji
	}
	reactReq := v1.ReactRequest{
		Username: config.SignalAccount,
		Reaction: &v1.JsonReaction{
			Emoji:               emoji,
			TargetAuthor:        d.Quote.Author,
			TargetSentTimestamp: d.Quote.ID,
			Remove:              remove,
		},
	}

	if d.Group == "" {
		reactReq.RecipientAddress = d.Quote.Author
	} else {
		reactReq.RecipientGroupID = d.Group
	}

	_, err := reactReq.Submit(signaldConn)
	if err != nil {
		return err
	}

	return nil
}

func (d *downloadJob) reply(s v1.SendRequest) error {
	s.Username = config.SignalAccount

	if d.Quote.ID > 0 {
		s.Quote = &d.Quote
	}

	if d.Group == "" {
		s.RecipientAddress = d.Quote.Author
	} else {
		s.RecipientGroupID = d.Group
	}

	_, err := s.Submit(signaldConn)

	return err
}

func (d *downloadJob) error(err error) {
	if d.Group != "" {
		// errors in a group tend to come from URLs that aren't meant to be downloaded
		// in that case just remove the reaction
		d.react("")
		return
	}

	d.react("⚠️")
	_ = d.reply(v1.SendRequest{
		MessageBody: fmt.Sprintf("⚠️ %s", err.Error()),
		Quote:       &d.Quote,
	})
}

func (d *downloadJob) cleanup() {
	err := os.RemoveAll(d.dir)
	if err != nil {
		logrus.Println("error cleaning up temporary directory", d.dir, err)
	}
}

func (d *downloadJob) mkdir() error {
	dir, err := ioutil.TempDir(os.TempDir(), "ydl-bot-")
	if err != nil {
		return err
	}
	os.Chmod(dir, 0755)
	d.dir = dir
	return nil
}

func downloadThumbnailURL(source string, dest string) error {
	f, err := os.Create(dest)
	if err != nil {
		return err
	}
	defer f.Close()

	resp, err := http.Get(source)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	_, err = io.Copy(f, resp.Body)
	if err != nil {
		return err
	}

	return nil
}
