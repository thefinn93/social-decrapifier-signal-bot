package bot_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/thefinn93/social-decrapifier-signal-bot/bot"
)

func TestSanatize(t *testing.T) {
	cases := map[string]string{
		"https://youtu.be/BaW_jenozKc?si=srwrhO22x7cprgRJ":                                  "https://youtu.be/BaW_jenozKc",
		"https://youtu.be/BaW_jenozKc":                                                      "https://youtu.be/BaW_jenozKc",
		"https://example.org?utm_source=abc&utm_id=aaaa&q=this+one+is+required&utm_medium=": "https://example.org?q=this+one+is+required",
	}

	for input, expected := range cases {
		assert.Equal(t, expected, bot.SanatizeURL(input))
	}
}
