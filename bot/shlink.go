package bot

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"
)

func shortenURL(long string) (string, error) {
	if config.Shlink.Domain == "" {
		return long, nil
	}

	var reqBody bytes.Buffer
	err := json.NewEncoder(&reqBody).Encode(map[string]string{
		"longUrl":    long,
		"domain":     config.Shlink.Domain,
		"validUntil": time.Now().Add(config.S3.PresignedLinkTTL.Duration).Format(time.RFC3339),
	})
	if err != nil {
		return "", err
	}

	req, err := http.NewRequest(http.MethodPost, fmt.Sprintf("https://%s/rest/v3/short-urls", config.Shlink.Domain), &reqBody)
	if err != nil {
		return "", err
	}

	req.Header.Add("X-Api-Key", config.Shlink.APIKey)
	req.Header.Add("User-Agent", "https://gitlab.com/thefinn93/social-decrapifier-signal-bot")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("unexpected %s from shlink instance (%s)", resp.Status, config.Shlink.Domain)
	}

	var respBody struct {
		ShortURL string `json:"shortUrl"`
	}
	if err := json.NewDecoder(resp.Body).Decode(&respBody); err != nil {
		return "", err
	}

	if respBody.ShortURL == "" {
		return "", errors.New("response from shlink did not include key shortUrl")
	}

	return respBody.ShortURL, nil
}
