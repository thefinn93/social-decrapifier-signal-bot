package bot

import (
	"os/exec"
	"time"

	log "github.com/sirupsen/logrus"
)

// UpdateYtdlp updates ytdlp every few days to keep things running smoothly
func UpdateYtdlp() {
	for {
		log.Info("updating yt-dlp. current version: ", ytdlpVersion())
		cmd := exec.Command("pip", "install", "-U", "git+https://github.com/yt-dlp/yt-dlp")
		_, err := cmd.Output()
		if err != nil {
			log.Error("error updating yt-dlp: ", err)
		}
		log.Info("update succeeded, now running yt-dlp ", ytdlpVersion())

		time.Sleep(time.Hour * 24)
	}
}
