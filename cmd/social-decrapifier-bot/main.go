package main

import (
	"fmt"
	"runtime"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/thefinn93/social-decrapifier-signal-bot/bot"
)

var repoName = "youtube-dl-queue"

func main() {
	log.SetReportCaller(true)
	log.SetFormatter(&log.TextFormatter{
		FullTimestamp:             true,
		EnvironmentOverrideColors: true,
		CallerPrettyfier: func(f *runtime.Frame) (string, string) {
			filename := f.File

			filenameParts := strings.SplitN(f.File, repoName+"/", 2)
			if len(filenameParts) > 1 {
				filename = filenameParts[1]
			} else {
				filenameParts = strings.Split(f.File, "/")
				filename = filenameParts[len(filenameParts)-1]
			}
			filename = fmt.Sprintf(" %s:%d", filename, f.Line)
			s := strings.Split(f.Function, ".")
			funcname := fmt.Sprintf("%s()", s[len(s)-1])
			return funcname, filename
		},
	})

	if err := bot.LoadConfig(); err != nil {
		log.Fatal("error loading config: ", err)
	}

	go bot.UpdateYtdlp()
	go bot.DownloadQueue()
	go bot.StartHTTPListener()
	bot.SignalListener()
}
