package bot

import (
	"errors"
	"net/http"
	"strings"

	echo "github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	log "github.com/sirupsen/logrus"
	v1 "gitlab.com/signald/signald-go/signald/client-protocol/v1"
)

func StartHTTPListener() {
	if config.HttpBind == "" {
		return
	}

	e := echo.New()
	e.HideBanner = true
	// e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.POST("/api/job", restAddJob)

	err := e.Start(config.HttpBind)
	if err != nil {
		log.Error("error starting http listener: ", err)
	}
}

func restAddJob(c echo.Context) error {
	u := c.FormValue("url")
	notifyUser := c.FormValue("notify_user")
	notifyGroup := c.FormValue("notify_group")

	if notifyUser != "" && notifyGroup != "" {
		return errors.New("notify_user and notify_group are incompatible: please specify just one")
	}

	j := downloadJob{URL: u}

	if notifyUser != "" {
		j.Quote.Author = &v1.JsonAddress{}
		if strings.HasPrefix(notifyUser, "+") {
			j.Quote.Author.Number = notifyUser
		} else {
			j.Quote.Author.UUID = notifyUser
		}
	} else if notifyGroup != "" {
		j.Group = notifyGroup
	} else {
		return errors.New("notify_user or notify_group required")
	}

	addToQueue(j)

	return c.NoContent(http.StatusNoContent)
}
