package bot

import (
	"encoding/json"
	"os"
	"runtime/debug"
	"strings"
	"time"

	minio "github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	log "github.com/sirupsen/logrus"
)

type configuration struct {
	SignalAccount       string            `json:"signal_account"`
	S3                  s3Config          `json:"s3"`
	MaxAttachmentSizeMB int64             `json:"max_attachment_size_mb"`
	SkipCleanup         bool              `json:"skip_cleanup"`        // SkipCleanup skips deleting the files on disk after download, useful for debugging
	ExtraYtdlpOptions   []string          `json:"extra_ytdlp_options"` // ExtraYtdlpOptions are passed to every yt-dlp invocation
	HttpBind            string            `json:"http_bind"`           // start http listener if set
	Debug               bool              `json:"debug"`
	SkipDomains         []string          `json:"skip_domains"` // list of domains not to attempt to download from
	ExtraFooters        map[string]string `json:"extra_footers"`
	Shlink              shlinkConfig      `json:"shlink"` // optional shlink (https://shlink.io) instance to use to shorten sent URLs
}

type s3Config struct {
	AccessKeyID      string   `json:"access_key_id"`
	SecretAccessKey  string   `json:"secret_access_key"`
	Endpoint         string   `json:"endpoint"`
	Bucket           string   `json:"bucket"`
	PublicURLBase    string   `json:"public_url_base"`
	PresignedLinkTTL duration `json:"presigned_link_ttl"`
}

type shlinkConfig struct {
	Domain string `json:"domain"`
	APIKey string `json:"api_key"`
}

type duration struct{ time.Duration }

var (
	config = configuration{
		MaxAttachmentSizeMB: 95,
		ExtraFooters: map[string]string{
			"source": "https://gitlab.com/thefinn93/social-decrapifier-signal-bot",
		},
		S3: s3Config{
			PresignedLinkTTL: duration{time.Hour},
		},
	}
	minioClient *minio.Client
)

// LoadConfig loads the config from a file on disk
func LoadConfig() error {
	f, err := os.Open("social-decrapifier-bot.json")
	if err != nil {
		return err
	}
	defer f.Close()

	err = json.NewDecoder(f).Decode(&config)
	if err != nil {
		return err
	}

	footer = buildFooter()

	if config.Debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}

	if config.S3.Endpoint != "" {
		minioClient, err = minio.New(config.S3.Endpoint, &minio.Options{
			Creds:  credentials.NewStaticV4(config.S3.AccessKeyID, config.S3.SecretAccessKey, ""),
			Secure: true, // why is this not default?
		})
		if err != nil {
			return err
		}

		if config.S3.PublicURLBase == "" {
			log.Info("no public URL has been configured for S3 uploads, big ugly pre-signed download URLs will be used")
		}

		log.Info("s3 configured, downloads will be cached to configured S3 bucket")
	} else {
		log.Info("S3 disabled, to enable set s3.endpoint")
	}

	return nil
}

func buildFooter() string {
	values := config.ExtraFooters

	if buildInfo, ok := debug.ReadBuildInfo(); ok {
		values["compiler"] = buildInfo.GoVersion
		for _, s := range buildInfo.Settings {
			switch s.Key {
			case "vcs.revision":
				values["revision"] = s.Value
			case "vcs.time":
				values["revision_timestamp"] = s.Value
			case "vcs.modified":
				if s.Value != "false" {
					values["modified since commit"] = s.Value
				}
			}
		}
		values["sum"] = buildInfo.Main.Sum
	}

	var f strings.Builder
	f.WriteString("bot info:\n")
	for k, v := range values {
		if v == "" {
			continue
		}

		f.WriteString(k)
		f.WriteString(": ")
		f.WriteString(v)
		f.WriteRune('\n')
	}

	return f.String()
}

func (d *duration) UnmarshalJSON(b []byte) error {
	var v string
	if err := json.Unmarshal(b, &v); err != nil {
		return err
	}

	var err error
	d.Duration, err = time.ParseDuration(v)
	if err != nil {
		return err
	}

	return nil

}
