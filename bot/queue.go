package bot

import (
	"fmt"
	"runtime/debug"
	"time"

	log "github.com/sirupsen/logrus"
)

var (
	downloadQueue = make(chan downloadJob, 100)
	inprogress    = false
)

// DownloadQueue processes jobs from the queue in order
func DownloadQueue() {
	go func() {
		for {
			if len(downloadQueue) > 0 {
				log.Info("working through download queue. current queue size: ", len(downloadQueue))
			}
			time.Sleep(time.Minute * 5)
		}
	}()

	for {
		log.Infof("working on next job in queue (%d)", len(downloadQueue))
		job := <-downloadQueue
		inprogress = true
		doJob(job)
		inprogress = false
	}
}

func doJob(job downloadJob) {
	defer func() {
		r := recover()
		if r == nil {
			return
		}

		log.Error("download job crashed: ", r)
		debug.PrintStack()

		job.error(fmt.Errorf("unexpected error: %v", r))
	}()

	if err := job.Download(); err != nil {
		log.Warn("download error: ", err)
		job.error(err)
	}
}

func addToQueue(job downloadJob) {
	downloadQueue <- job
	log.Infof("job added to queue (%d)", len(downloadQueue))
}
